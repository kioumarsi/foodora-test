<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
require "vendor/autoload.php";
use Lib\Jobs\SpecialDayFix;
use Lib\Jobs\SpecialDayRevert;

// get the parameter we passed for each command
if(isset($argv[2]))
    $parameter = substr_replace($argv[2],'', 0,2 );
else
    $parameter = null;

// get the command and instantiate the job object
switch($argv[1]){
    case "updateSchedule":
        $job = new SpecialDayFix($parameter);
        break;
    case "revert":
        $job = new SpecialDayRevert($parameter);break;
}

//prepare the job if there are somethings to do before running the job continuously
$job->prepare();

//Run the job while whole the process is finished and rest 1 second in between
while(!$job->isFinished()){
    $job->run();
    sleep(1);
}

