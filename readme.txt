*************** IMPORTANT NOTE ***********
This application is tested with 40.000 records and should work smoothly (running in few seconds).
In case of any problem during running the application please contact me to figure out what is wrong.

In purpose to be fast in high volume data conversion I droped the table and insert all the data
instead of updating current data


**************** Setup ****************
run php composer.phar update
in JobAbstract.php file in '/var/www/html/foodora/Lib/Jobs/ folder' :
- set $absolutePath to desire address (it could load from a config file or get automatically).
- make sure the 'backup' folder and  '/var/www/html/foodora/Lib/Jobs/logs' folder has write permission.

**************** Run ****************
you can use these commands from terminal from project root folder to use the system

these will update the venture_schedule table with special dates of asked week:
php commands/specialDaysFix.php updateSchedule
php commands/specialDaysFix.php updateSchedule  --force

* you cannot override the back up table by running the command several times. so you can use
--force parameter to force the script override the table backup


This should be run to revert back the data after the problem is fixed:
php commands/specialDaysFix.php revert




**************** Debug ****************
if you continue receiving this message "The process is already running" :
- delete   ''/var/www/html/foodora/Lib/Jobs/logs manually'


