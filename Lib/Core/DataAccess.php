<?php
namespace Lib\Core;

/**
 * Class DataAccess
 * @package Lib\Core
 * simple data access class to use the pdo inside project
 */
class DataAccess {

    /**
     * @var string
     */
    private static $host = 'localhost';

    /**
     * @var string
     */
    private static $dbname = 'foodora-test';

    /**
     * @var string
     */
    private static $user = 'dev';

    /**
     * @var string
     */
    private static $password = 'ubuntu';

    /**
     * @var \PDO
     */
    private static $instance;

    /**
     * @return \PDO
     * get the pdo instance and make sure we have only one instance instantiated 
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new \PDO('mysql:host='.self::$host.';dbname='.self::$dbname,self::$user,self::$password);
            static::$instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }

        return static::$instance;
    }

    /**
     * DataAccess constructor.
     * to be sure nobody can instantiate the class outside the class
     */
    protected function __construct()
    {
    }
    
}