<?php
namespace Lib\Jobs;
use Lib\Core\DataAccess;
use Lib\Query\VendorScheduleQuery;
use Lib\Query\VendorSpecialDayQuery;

/**
 * Class SpecialDayFix
 * @package Lib\Jobs
 * This class will read the special days from 2015-12-20 to 2015-12-28 and
 * update the vendors schedule with the schedule of that period for each day of week.
 */
class SpecialDayFix extends JobAbstract{

    /**
     * @var int
     * number of rows we convert per each run call
     */
    private $limit = 10000;

    /**
     * @var string
     */
    protected $name = 'SpecialDayFix';

    /**
     * @return bool
     * 
     */
    public function prepare(){

        $file = $this->getBackupFile();
        $this->log('getting vendor_schedule table backup ...');
        // dump tabke back up to the csv file
        DataAccess::getInstance()->query("SELECT * INTO OUTFILE '$file' FROM `vendor_schedule`");
        // log the current id to zero
        file_put_contents('Lib/Jobs/logs/SpecialDayFix.log',0);

        //truncate the table if the backup is done to be ready to insert the data
        if(filesize($file) >0) {
            VendorScheduleQuery::truncate();
            return true;
        }else{
            /**
             * exit the process if couldn't expoert the data. it could be check more proper
             * by counting the rows and be sure all the data is really exported
            **/
            $this->log('couldn\'t export the table ');
            return false;
        }

    }

    /**
     * @return bool
     * run the process of conversion and updating the venture schedule temporary
     */
    public function run(){
        $this->lock();
        $this->log('Inserting to the databse...');
        $startId = intval(file_get_contents('Lib/Jobs/logs/SpecialDayFix.log'));
        if(! $specialDays = $this->getSpecialDays($startId)){
            $this->unLock();
            $this->finish();
        }
        $currentId = $startId;
        $this->createInsertRows($specialDays,$currentId);
        $rows = $this->createInsertRows($specialDays, $currentId);
        $file = $this->insertRowsToCSVFile($rows);
        // import data to database from csv file
        
        DataAccess::getInstance()->query("LOAD DATA INFILE '$file' INTO TABLE `vendor_schedule`");
        $this->log( count($rows). " rows inserted to the databse");
        //update the current id to be used in next run call
        file_put_contents('Lib/Jobs/logs/SpecialDayFix.log',$startId+$this->limit);
        $this->unLock();
        return true;
    }

    /**
     * @return string
     * @return string | exit
     * check if the backup file exist and exit unless it is forced then return back the file name
     * 
     */
    private function getBackupFile(){
        $file = $this->getAbsolutePath().'backup/vendor_schedule.csv';
        if(is_file($file)){
            if($this->getParameter() == 'force'){
                unlink($file);
            }else {
                $this->log('The vendor_schedule.csv file exists. If you are sure you want to override it run this command');
                $this->log('php commands/specialDaysFix.php updateSchedule --force');
                exit;
            }
        }
        return $file;
    }

    /**
     * @param $specialDays
     * @param $currentId
     * @return array
     * get the specialdays and create and array which is used to insert to the csv file
     */
    private function createInsertRows($specialDays,$currentId){
        $rows = [];
        foreach($specialDays as $specialDay) {
            $currentId ++;
            $dateTimeStamp = strtotime($specialDay->getSpecialDate());
            $weekDay = date("N", $dateTimeStamp);
            $rows[] = [
                $currentId,
                $specialDay->getVendorId(),
                $weekDay,
                $specialDay->isAllDay(),
                $specialDay->getStartHour(),
                $specialDay->getStopHour()
            ];
        }
        
        return $rows;
    }

    /**
     * @param $rows
     * @return string
     */
    private function insertRowsToCSVFile($rows){
        $file = $this->getAbsolutePath().'backup/vendor_schedule_temp.csv';
        $fp = fopen($file, 'w');
        foreach ($rows as $row) {
            fputcsv($fp, $row, "\t");
        }
        fclose($fp);
        return $file;
    }

    /**
     * @param $startId
     * @return bool
     * read specialdays from database
     */
    private function getSpecialDays($startId){
        $specialDays = VendorSpecialDayQuery::create()
        ->filterByDateRange('special_date',array('2015-12-20','2015-12-28'))
        ->filterBy('event_type','opened')
        ->limit($this->limit,$startId)
        ->findAll();
        
        if(empty($specialDays))
            return false;
        else
            return $specialDays;
    }
    
    
}