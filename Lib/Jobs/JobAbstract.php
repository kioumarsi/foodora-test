<?php

namespace Lib\Jobs;

/**
 * Class JobAbstract
 * @package Lib\Jobs
 * All jobs should extend this class
 */
abstract Class JobAbstract {

    /**
     * @var string
     * Change the address to anywhere you want to save the backups
     */
    private $absolutePath = '/var/www/html/foodora/';

    /**
     * @var bool
     */
    private $finished = false;

    /**
     * @var string
     * the argument that is to have different options to run the same job
     */
    private $parameter;

    /**
     * @var string
     */
    private $lockFile;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return true | \Exception
     * them main public function to run the job
     */
    abstract public function run();

    /**
     * JobAbstract constructor.
     * @param null $parameter
     * set the log file based on job name
     */
    public function __construct($parameter = null) {
        $this->parameter = $parameter;
        $this->lockFile = 'Lib/Jobs/logs/'.$this->name.'.lock';
    }

    /**
     * @return bool
     * to have no preparation by default . So we can always call the prepare function when running a job
     * and follow the same procedure for all jobs
     */
    public function prepare(){
        return true;
    }

    /**
     * @return bool
     * check if the job is finished
     */
    public function isFinished(){
        return $this->finished;
    }

    /**
     * @param $text
     * so later on we can have more complex logging if needed
     */
    protected function log($text){
        echo $text."\n";
    }

    /**
     * @return bool
     * do whatever needed when a job is finished and exit
     */
    protected function finish(){
        $this->finished = true;
        $this->log('finished!');
        exit;
    }

    /**
     * @return string
     */
    protected function getAbsolutePath(){
        return $this->absolutePath;
    }

    /**
     * @return null|string
     */
    protected function getParameter(){
        return $this->parameter;
    }

    /**
     * create lock file and check if the job is locked
     * @return exit | true
     */
    protected function lock(){
        if(is_file($this->lockFile)){
            $this->log("The process is already running");
            exit;
        }else{
            touch($this->lockFile);
        }

        return true;
    }

    /**
     * remove the lock file to make the job unlocked to call again
     */
    protected function unLock(){
        unlink($this->lockFile);
    }
}