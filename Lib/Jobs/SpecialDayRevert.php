<?php
namespace Lib\Jobs;
use Lib\Core\DataAccess;
use Lib\Query\VendorScheduleQuery;

/**
 * Class SpecialDayRevert
 * @package Lib\Jobs
 * revert the vendor_schedule table data after fixing the bug
 */
class SpecialDayRevert extends JobAbstract{

    /**
     * @var string
     */
    protected $name = 'SpecialDayRevert';

    /**
     *  import data from csv backup file
     */
    public function run(){
        $this->lock();
        $file = $this->getAbsolutePath().'backup/vendor_schedule.csv';
        $this->log('truncating current table ...');
        VendorScheduleQuery::truncate();
        $this->log('loading the back up table ...');
        DataAccess::getInstance()->query("LOAD DATA INFILE '$file' INTO TABLE `vendor_schedule`");
        $this->finish();
        $this->unLock();
    }
}