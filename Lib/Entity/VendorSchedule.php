<?php
/**
 * Created by PhpStorm.
 * User: sajad
 * Date: 05.12.16
 * Time: 12:49
 */

namespace Lib\Entity;



class VendorSchedule implements EntityInterface{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $vendor_id;

    /**
     * @var int
     */
    private $weekday;

    /**
     * @var boolean
     */
    private $all_day;

    /**
     * @var string
     */
    private $start_hour;

    /**
     * @var strign
     */
    private $stop_hour;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVendorId() {
        return $this->vendor_id;
    }

    /**
     * @param int $vendor_id
     */
    public function setVendorId($vendor_id) {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return int
     */
    public function getWeekday() {
        return $this->weekday;
    }

    /**
     * @param int $weekday
     */
    public function setWeekday($weekday) {
        $this->weekday = $weekday;
    }

    /**
     * @return boolean
     */
    public function isAllDay() {
        return $this->all_day;
    }

    /**
     * @param boolean $all_day
     */
    public function setAllDay($all_day) {
        $this->all_day = $all_day;
    }

    /**
     * @return string
     */
    public function getStartHour() {
        return $this->start_hour;
    }

    /**
     * @param string $start_hour
     */
    public function setStartHour($start_hour) {
        $this->start_hour = $start_hour;
    }

    /**
     * @return strign
     */
    public function getStopHour() {
        return $this->stop_hour;
    }

    /**
     * @param strign $stop_hour
     */
    public function setStopHour($stop_hour) {
        $this->stop_hour = $stop_hour;
    }
    
    

}