<?php
/**
 * Created by PhpStorm.
 * User: sajad
 * Date: 05.12.16
 * Time: 12:49
 */

namespace Lib\Entity;


use Lib\Core\BaseEntity;

/**
 * Class Vendor
 * @package Lib\Entity
 * 
 */
class Vendor implements EntityInterface{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }
    
}