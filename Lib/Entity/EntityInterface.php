<?php
/**
 * Created by PhpStorm.
 * User: sajad
 * Date: 07.12.16
 * Time: 08:29
 */

namespace Lib\Entity;

/**
 * Interface EntityInterface
 * @package Lib\Entity
 * @Date    ${DATE}
 * To be sure all entites has id as we rely on table id in our queries
 */
interface EntityInterface {
    
    public function getId();
    public function setId($id);
}