<?php
/**
 * Created by PhpStorm.
 * User: sajad
 * Date: 06.12.16
 * Time: 10:20
 */

namespace Lib\Entity;


class VendorSpecialDay implements EntityInterface{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $vendor_id;

    /**
     * @var int
     */
    private $special_date;

    /**
     * @var string
     */
    private $event_type;

    /**
     * @var boolean
     */
    private $all_day;

    /**
     * @var string
     */
    private $start_hour;

    /**
     * @var string
     */
    private $stop_hour;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getVendorId() {
        return $this->vendor_id;
    }

    /**
     * @param int $vendor_id
     */
    public function setVendorId($vendor_id) {
        $this->vendor_id = $vendor_id;
    }

    /**
     * @return int
     */
    public function getSpecialDate() {
        return $this->special_date;
    }

    /**
     * @param int $special_date
     */
    public function setSpecialDate($special_date) {
        $this->special_date = $special_date;
    }

    /**
     * @return string
     */
    public function getEventType() {
        return $this->event_type;
    }

    /**
     * @param string $event_type
     */
    public function setEventType($event_type) {
        $this->event_type = $event_type;
    }

    
    /**
     * @return boolean
     */
    public function isAllDay() {
        return $this->all_day;
    }

    /**
     * @param boolean $all_day
     */
    public function setAllDay($all_day) {
        $this->all_day = $all_day;
    }

    /**
     * @return string
     */
    public function getStartHour() {
        return $this->start_hour;
    }

    /**
     * @param string $start_hour
     */
    public function setStartHour($start_hour) {
        $this->start_hour = $start_hour;
    }

    /**
     * @return string
     */
    public function getStopHour() {
        return $this->stop_hour;
    }

    /**
     * @param string $stop_hour
     */
    public function setStopHour($stop_hour) {
        $this->stop_hour = $stop_hour;
    }

  
}