<?php

namespace Lib\Query;
use Lib\Core\DataAccess;

/**
 * Class BaseQuery
 * @package Lib\Query
 * the base class that all query classes extend it. 
 * it gives the ability to query tables with some filter functionality
 * and all critical database actions should be added later
 */
class BaseQuery {

    protected $tableName;
    protected $entityName;
    private $query;
    private $bindParams = [];


    /**
     * @param string $fields
     * @return object BaseQuery
     * create the query class base on table name
     */
    public static function create($fields = '*'){
        $className =  \get_called_class();
        $instance = new $className;
        $instance->query = "SELECT $fields FROM ".$instance->getTableName() ;
        return $instance;
    }

    /**
     * @param $entity
     * delete a row from the table
     */
    public static function delete($entity){
        $className =  \get_called_class();
        $instance = new $className;
        $result = DataAccess::getInstance()->query('DELETE FROM '.$instance->getTableName().' WHERE id ='.$entity->getId());
        $result ? true : false;
    }

    /**
     * truncate 
     */
    public static function truncate(){
        $className =  \get_called_class();
        $instance = new $className;
        $result = DataAccess::getInstance()->query('TRUNCATE  '.$instance->getTableName());
        $result ? true : false;

    }



    public function getTableName(){
        return $this->tableName;
    }

    public function findAll(){
        $statement = DataAccess::getInstance()->prepare($this->query);
        $statement->execute($this->bindParams);
        $entities= [];
        while($object = $statement->fetchObject()){
            $class = 'Lib\Entity\\'.$this->entityName;
            $entity = new $class;
            $entities[] = self::cast($entity,$object );
        }

        return $entities;
    }

    public function findOne(){
        return $this->findAll()[0];
    }

    public function filterBy($field,$value){
        $valueParam = ':'.$field.'Value';
        $this->query.=" ".$this->getConjunction()." $field = $valueParam";
        $this->bindParams[$valueParam] = $value;
        return $this;

    }


    public function filterByDateRange($field,Array $range){
        $this->query.=" ".$this->getConjunction()." $field BETWEEN :from and :to ";
        $this->bindParams['from'] = $range[0];
        $this->bindParams['to'] =  $range[1];
        return $this;
    }

    public function limit($limit,$start=0){
        $start = intval($start);
        $limit = intval($limit);
        $this->query.=" limit $start,$limit ";
            return $this;
    }

    private function cast($destination, $sourceObject) {
        if (is_string($destination)) {
            $destination = new $destination();
        }
        $sourceReflection = new \ReflectionObject($sourceObject);
        $destinationReflection = new \ReflectionObject($destination);
        $sourceProperties = $sourceReflection->getProperties();
        foreach ($sourceProperties as $sourceProperty) {
            $sourceProperty->setAccessible(true);
            $name = $sourceProperty->getName();
            $value = $sourceProperty->getValue($sourceObject);
            if ($destinationReflection->hasProperty($name)) {
                $propDest = $destinationReflection->getProperty($name);
                $propDest->setAccessible(true);
                $propDest->setValue($destination, $value);
            } else {
                $destination->$name = $value;
            }
        }
        return $destination;
    }

    private function getConjunction(){
        if(strstr($this->query,'WHERE' ))
            return 'AND';
        else
            return 'WHERE';
    }
}