<?php
/**
 * Created by PhpStorm.
 * User: sajad
 * Date: 05.12.16
 * Time: 13:03
 */

namespace Lib\Query;


use Lib\Core\DataAccess;
use Lib\Entity\Vendor;
use Lib\Query\BaseQuery;

class VendorQuery extends BaseQuery{

     protected $entityName = 'Vendor';
     protected $tableName = 'vendor';

     
}